##Загалом по проекту:
* www - корінь веб сервера
* dump - скрипти для створення/розгортання дампу данних з сайту

* protected/components
	* ArrangedBehavior.php - поведінка для моделей впорядкованих колекцій
	* FormattedDateFieldBehavior.php - поведінка для сворення поля для вводу дати з нестандартним форматуванням
	* RelatedIdsFieldBehavior.php - поведіка для управління поростими many-to-many звязками
	* SiteMapWriter.php - клас для генерації карти сайту
	
* modules/manager - код модуля для контентом на сайті

### Конфігурація проекту
	*.php.dist - шаблони кофігураційних файлів
	(файли з специфічними для сервера параметрами наприклад пароль бази даних не повинні додаватись в репозиторій) 

##По адмін панелі
Модуль не сильно документований, і місцями  мені не сильно подобається - проте, з для своєї цілї він підходить :) Тому постараюсь описати що і як там працює… 


Модуль адмінки включає в себе:

- окрему систему користувачів та інтерфейс для управління ними
- організацію навігації по адмін частині
- шаблони для Gii
	- bcrud (шблони для управління колекціями - там є два набори шаблонів: default та oredered для звичайних та впорядкованих колекцій)
- компоненти для типових завданнь
	- StaticPageEditor.php - редагування данних статичних сторінок
 	- SlugValidator.php - валідація "slug"
	- SlugInput.php - поле для вводу "slug" з автозаповненням на основі транслітенації іншого поля
	- SettingsEditor.php - редактор налаштуваннь
	- RCrudController.php - базовий класс контроллера для управління впорядкованими колекцій									 
	- DataModel.php - модель данних за конфігурацією(використовується наприклад при редагуванні статичних сторінок, чи налаштуваннь)
	- FormWidget.php - віджет для генерації форм за конфігурацією
	- модуль для перекладу фраз на сайті, в цьому проекті навряд пригодиться, але він є і коли потрібно - значно спрощує управління перекладами повідомленнь на сайті
	- модуль з прикладами використання різних віджетів (playground)

В оформленні використовуеться Twitter Bootstrap 2.3, ніякі розширення для інтеграції з бутстрепом не використовуються, проте є деякі допоміжні віджети для зручності роботи.

 - BGridView.php
 - BHorizontalForm.php
 - BHorizontalMenu.php
 - BLinkPager.php
 - BSideMenu.php
 - BTabs.php


### Навігація
Адмін панель формується з модулів пронаслідуваних від AdminExtModule кожен з них повинен повертати набір пунктів для головного меню адмін панелі в методі menuConfig()

В ManagerModule є приклад, того як це може вигядати. Також в AdminModule в коментарях я постарався описати формат кофігурації пунктів головного меню.

###  Статичні сторінки 
Редагування статичних сторінок реалізоване в классі StaticPageEditor, приклади використання є в самому класі та в SitePageController.


### Користувачі за замовчуванням
* admin: password
* developer: password
 
