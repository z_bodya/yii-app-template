<?php

class m131207_070000_add_gallery_extension_property extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{gallery}}', 'extension', 'VARCHAR(10) NOT NULL DEFAULT \'jpg\'');
    }

    public function down()
    {
        $this->dropColumn('{{gallery}}', 'extension');
    }

}