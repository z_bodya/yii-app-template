<?php

class m130123_200914_base extends CDbMigration
{
    public function up()
    {
        //return true;
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `{{admin_auth_assignment}}` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `{{admin_auth_item}}` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `{{admin_auth_item_child}}` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `{{admin_user}}` (
  `username` varchar(64) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{{admin_auth_item}}` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Администратор', NULL, 'N;'),
('authenticated', 2, 'Пользователь', NULL, 'N;'),
('developer', 2, 'Разработчик', NULL, 'N;');

INSERT INTO `{{admin_auth_item_child}}` (`parent`, `child`) VALUES
('developer', 'admin');

INSERT INTO `{{admin_user}}` (`username`, `password`, `salt`, `name`) VALUES
('admin', 'cf373489fa2425711ba9526f204251bf1b30da3d', 'uqLt5vcZ2V', 'Администратор'),
('developer', '20239caa272a13eb425afb4b5f4d6faaab198765', 'mbnjdHReOf', 'Разработчик');

INSERT INTO `{{admin_auth_assignment}}` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 'admin', NULL, 'N;'),
('developer', 'developer', NULL, 'N;');

ALTER TABLE `{{admin_auth_assignment}}`
  ADD CONSTRAINT `{{admin_auth_assignment}}_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `{{admin_auth_item}}` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `{{admin_auth_item_child}}`
  ADD CONSTRAINT `{{admin_auth_item_child}}_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `{{admin_auth_item}}` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `{{admin_auth_item_child}}_ibfk_2` FOREIGN KEY (`child`) REFERENCES `{{admin_auth_item}}` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `{{static_page}}` (
  `section` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `data` longtext,
  `title` varchar(512) DEFAULT NULL,
  `description` text,
  `keywords` text,
  PRIMARY KEY (`section`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function down()
    {
        $this->execute(<<<SQL
DROP TABLE IF EXISTS `{{admin_auth_assignment}}`;
DROP TABLE IF EXISTS `{{admin_auth_item_child}}`;
DROP TABLE IF EXISTS `{{admin_auth_item}}`;
DROP TABLE IF EXISTS `{{admin_auth_item}}`;
DROP TABLE IF EXISTS `{{admin_user}}`;
DROP TABLE IF EXISTS `{{static_page}}`;
SQL
        );
        return true;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}