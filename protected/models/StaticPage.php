<?php

/**
 * This is the model class for table "static_page".
 *
 * The followings are the available columns in table 'static_page':
 * @property string $section
 * @property string $key
 * //property string $link
 * @property string $name
 * @property string $data
 * @property string $title
 * @property string $description
 * @property string $keywords
// * @property string $last_change
 * // * @property double $priority
 * // * @property string $change_freq
 */
class StaticPage extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return StaticPage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{static_page}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('section, key' /*link*/, 'required'),
            //array('priority', 'numerical'),
            array('section, key' /*link*/, 'length', 'max' => 64),
            array('name', 'length', 'max' => 256),
            array('title', 'length', 'max' => 512),
            array('data, description, keywords' /* last_change, change_freq */, 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('section, key, name, data, title, description, keywords' /*last_change, priority, change_freq, link*/, 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'section' => 'Раздел сайта куда относится страница',
            'key' => 'Уникальный ключ страницы в разделе',
            // 'link' => 'Плевдоным страницы',
            'name' => 'Название',
            'data' => 'Data',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'keywords' => 'Ключевые слова',
            // 'last_change' => 'Дата последнего редактирования',
            // 'priority' => 'Относительный приоритет страницы',
            // 'change_freq' => 'Частота редактырования',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('section', $this->section, true);
        $criteria->compare('key', $this->key, true);
        // $criteria->compare('link', $this->link, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('data', $this->data, true);

        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('keywords', $this->keywords, true);
        //  $criteria->compare('last_change', $this->last_change, true);
        //  $criteria->compare('priority', $this->priority);
        //  $criteria->compare('change_freq', $this->change_freq, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    private $_data = null;

    /**
     * @return array
     */
    public function getDataAttributes()
    {
        if (!isset($this->_data)) {
            $this->_data = unserialize($this->data);
        }
        if (!is_array($this->_data)) $this->_data = array();
        return $this->_data;
    }

    /**
     * @param array $data
     */
    public function setDataAttributes($data)
    {
        $this->_data = $data;
        $this->data = serialize($data);
    }
}