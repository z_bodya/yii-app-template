<?php

/**
 * Adds field to manage many-to-many relation
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 *
 * Example configuration, for models:
 *
 *      Picture(id),
 *      PictureCategory(id),
 *      PictureHasCategory(picture_id, picture_category_id)
 *
 *      'pictureCategoryIdsBehavior' => array(
 *          'class' => 'RelatedIdsFieldBehavior',
 *          'modelName' => 'PictureHasCategory',
 *          'relatedId' => 'picture_category_id',
 *          'ownerId' => 'picture_id',
 *          'relatedModelId' => 'id',
 *          'modelId' => 'id',
 *          'fieldName' => 'pictureCategoryIds',
 *          'relationName' => 'pictureCategories',
 *      ),
 */
class RelatedIdsFieldBehavior extends CActiveRecordBehavior
{
    /**
     * Name of model for many-to-many relation table
     * @var string
     */
    public $modelName;

    /**
     * Name of filed for related model id in relation table
     * @var string
     */
    public $relatedId;

    /**
     * Name of filed for owner model id in relation table
     * @var string
     */
    public $ownerId;

    /**
     * Name of id filed in related model
     * @var string
     */
    public $relatedModelId;

    /**
     * Name of id filed in host model
     * @var string
     */
    public $modelId;

    /**
     * Name of filed to add into model
     * @var string
     */
    public $fieldName;

    /**
     * Name of many-to-many relation in owner
     * @var string
     */
    public $relationName;

    private $_relatedIds;

    public function hasProperty($name)
    {
        if ($name == $this->fieldName) return true;
        return parent::hasProperty($name);
    }

    public function canGetProperty($name)
    {
        if ($name == $this->fieldName) return true;
        return parent::canGetProperty($name);
    }

    public function canSetProperty($name)
    {
        if ($name == $this->fieldName) return true;
        return parent::canSetProperty($name);
    }

    public function __get($name)
    {
        if ($name == $this->fieldName) {
            return $this->getRelatedIds();
        }
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if ($name == $this->fieldName) {
            $this->setRelatedIds($value);
        } else {
            parent::__set($name, $value);
        }
    }


    private function getRelatedIds()
    {
        if (!isset($this->_relatedIds)) {
            $res = array();
            foreach ($this->owner->{$this->relationName} as $related) {
                $res[] = $related->{$this->relatedModelId};
            }
            $this->_relatedIds = $res;
        }
        return $this->_relatedIds;
    }

    public function setRelatedIds($value)
    {
        if (is_array($value))
            $this->_relatedIds = $value;
    }

    public function afterSave($event)
    {
        parent::afterSave($event);
        $relatedModels = $this->owner->{$this->relationName};

        $relatedIds = array_flip($this->getRelatedIds());
        foreach ($relatedModels as $related) {
            if (!isset($relatedIds[$related->{$this->relatedModelId}])) {
                $this->getModel()->deleteByPk(array(
                    $this->ownerId => $this->owner->{$this->modelId},
                    $this->relatedId => $related->{$this->relatedModelId}
                ));
            }
        }
        $idsToAdd = $relatedIds;
        foreach ($relatedModels as $related) {
            if (isset($idsToAdd[$related->{$this->relatedModelId}]))
                unset($idsToAdd[$related->{$this->relatedModelId}]);
        }
        foreach ($idsToAdd as $id => $v) {
            $rel = $this->newRelation();
            $rel->{$this->ownerId} = $this->owner->{$this->modelId};
            $rel->{$this->relatedId} = $id;
            $rel->save();
        }
    }

    public function beforeDelete($event)
    {
        $this->getModel()->deleteAll($this->ownerId . ' = ' . $this->owner->{$this->modelId});
        parent::beforeDelete($event);
    }

    /**
     * @return CActiveRecord
     */
    private function getModel()
    {
        return CActiveRecord::model($this->modelName);
    }

    /**
     * @return CActiveRecord
     */
    private function newRelation()
    {
        return new $this->modelName;
    }
}
