<?php
/**
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 * For more info about params see:
 * @link http://www.sitemaps.org/protocol.html
 */
class SiteMapWriter
{
    const FREQ_ALWAYS = 0;
    const FREQ_HOURLY = 1;
    const FREQ_DAILY = 2;
    const FREQ_WEEKLY = 3;
    const FREQ_MONTHLY = 4;
    const FREQ_YEARLY = 5;
    const FREQ_NEVER = 6;
    private static $freq = array(
        'always',
        'hourly',
        'daily',
        'weekly',
        'monthly',
        'yearly',
        'newer');
    private $_pull = array();

    public function addPage($location, $lastmod = null, $changefreq = null, $priority = null)
    {
        $this->_pull[] = array(
            $location, $lastmod, $changefreq, $priority
        );
    }

    public function generateXml($processOutput = false)
    {
        if ($processOutput) {
            ob_start();
        }
        echo '<?xml version="1.0" encoding="UTF-8"?>',

        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach ($this->_pull as &$item) {
            echo '<url>', '<loc>', htmlentities($item[0]), '</loc>';
            if (isset($item[1])) {
                echo '<lastmod>', $item[1], '</lastmod>';
            }
            if (isset($item[1])) {
                echo '<changefreq>', self::$freq[$item[2]], '</changefreq>';

            }
            if (isset($item[1])) {
                echo '<priority>', $item[3], '</priority>';
            }
            echo '</url>';
        }
        echo '</urlset>';
        $res = null;
        if ($processOutput) {
            $res = ob_get_clean();
        }
        return $res;
    }
}
