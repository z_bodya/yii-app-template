<?php
/**
 * TwigViewRenderer component
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 *
 * Twig official site
 * @link http://twig.sensiolabs.org
 */
class TwigViewRenderer extends CApplicationComponent implements IViewRenderer
{
    public $fileExtension = '.twig';

    public $options = array();

    public $templatePaths = array();

    /** @var Twig_Environment */
    private $_twig;

    private $_basePath;
    private $_basePathLength;

    private $_themeBasePath;
    private $_themeBasePathLength;

    /**
     * Renders a view file.
     * @param CBaseController $context the controller or widget who is rendering the view file.
     * @param string $file the view file path
     * @param mixed $data the data to be passed to the view
     * @param boolean $return whether the rendering result should be returned
     * @return mixed the rendering result, or null if the rendering result is not needed.
     */
    public function renderFile($context, $file, $data, $return)
    {
        $data['this'] = $context;
        $theme = Yii::app()->theme;
        if (isset($theme)) {
            if ($theme->basePath != $this->_themeBasePath)
                $this->initTwig();
        } else {
            if (isset($this->_themeBasePath))
                $this->initTwig();
        }

        if (isset($this->_themeBasePath) && substr($file, 0, $this->_basePathLength) != $this->_basePath) {
            $sourceFile = substr($file, $this->_themeBasePathLength);
        } else {
            $sourceFile = substr($file, $this->_basePathLength);
        }

        $result = $this->_twig->loadTemplate($sourceFile)->render($data);
        if ($return) {
            return $result;
        } else {
            echo $result;
        }
        return null;
    }

    public function init()
    {
        //  require  dirname(__FILE__) . '/vendor/Twig/lib/Twig/Autoloader.php';
        //  Yii::registerAutoloader(array('Twig_Autoloader', 'autoload'), true);

        $app = Yii::app();
        $this->_basePath = $app->getBasePath() . '/views';
        $this->_basePathLength = strlen($this->_basePath);


        $this->initTwig();
        parent::init();
    }

    public function getEnvironment()
    {
        return $this->_twig;
    }

    private function initTwig()
    {
        $app = Yii::app();
        $theme = $app->getTheme();
        if (isset($theme) && file_exists($theme->basePath . '/views')) {
            $this->_themeBasePath = $theme->basePath . '/views';
            $this->_themeBasePathLength = strlen($this->_themeBasePath);
            $loader = new Twig_Loader_Filesystem(array($this->_themeBasePath, $this->_basePath));
        } else {
            $loader = new Twig_Loader_Filesystem($this->_basePath);
        }
        $options = array(
            'debug' => YII_DEBUG,
            'cache' => $app->getRuntimePath() . '/twigCache',
            'charset' => $app->charset,
            'strict_variables' => false,
        );

        foreach ($this->templatePaths as $name => $alias) {
            $loader->addPath(Yii::getPathOfAlias($alias), $name);
        }

        $this->_twig = new Twig_Environment($loader, array_merge($options, $this->options));
        if (YII_DEBUG) {
            $this->_twig->addExtension(new Twig_Extension_Debug());
        }

        $this->_twig->addGlobal('app', Yii::app());
        $this->_twig->addGlobal('html', new StaticClassProxy('CHtml'));
        $this->_twig->addGlobal('yii', new StaticClassProxy('Yii'));
    }

    public function  clearCache()
    {
        $this->_twig->clearCacheFiles();
        $this->_twig->clearTemplateCache();
    }

}

/**
 * Class-proxy for static classes
 * Needed because you can't pass static class to Twig other way
 *
 * @author Leonid Svyatov <leonid@svyatov.ru>
 * @version 1.0.0
 */
class StaticClassProxy
{
    private $_staticClassName;

    public function __construct($staticClassName)
    {
        $this->_staticClassName = $staticClassName;
    }

    public function __get($property)
    {
        return ${$this->_staticClassName}::$property;
    }

    public function __set($property, $value)
    {
        return (${$this->_staticClassName}::$property = $value);
    }

    public function __call($method, $arguments)
    {
        return call_user_func_array(array($this->_staticClassName, $method), $arguments);
    }
}