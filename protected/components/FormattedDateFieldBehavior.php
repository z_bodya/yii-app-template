<?php

/**
 * Behavior to add custom formatted date time field to model.
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 *
 * Example configuration:
 *
 *   'dateRuBehavior' => array(
 *        'class' => 'FormattedDateFieldBehavior',
 *        'fieldName' => 'dateRu',
 *        'fieldFormat' => 'dd.MM.yyyy hh:mm:ss',
 *        'modelFieldName' => 'date',
 *        'modelFieldFormat' => 'yyyy-MM-dd hh:mm:ss',
 *   )
 */
class FormattedDateFieldBehavior extends CActiveRecordBehavior
{

    public $fieldName;
    public $fieldFormat;

    public $modelFieldName;
    public $modelFieldFormat;

    public function hasProperty($name)
    {
        if ($name == $this->fieldName) return true;
        return parent::hasProperty($name);
    }

    public function canGetProperty($name)
    {
        if ($name == $this->fieldName) return true;
        return parent::canGetProperty($name);
    }

    public function canSetProperty($name)
    {
        if ($name == $this->fieldName) return true;
        return parent::canSetProperty($name);
    }

    public function __get($name)
    {
        if ($name == $this->fieldName) {
            return $this->getField();
        }
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if ($name == $this->fieldName) {
            $this->setField($value);
        } else {
            parent::__set($name, $value);
        }
    }

    private function getField()
    {
        $date = $this->getOwner()->{$this->modelFieldName};
        if ($date !== null)
            $timestamp = CDateTimeParser::parse($date, $this->modelFieldFormat);
        else
            $timestamp = time();
        return Yii::app()->dateFormatter->format($this->fieldFormat, $timestamp);
    }

    private function setField($value)
    {
        $timestamp = CDateTimeParser::parse($value, $this->fieldFormat);
        $this->getOwner()->{$this->modelFieldName} = Yii::app()->dateFormatter->format($this->modelFieldFormat, $timestamp);
    }
}
