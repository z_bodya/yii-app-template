<?php

class ArrangedBehavior extends CActiveRecordBehavior
{
    public $rankField = 'rank';
    public $defaultValueField = 'id';

    public function afterSave($event)
    {
        if ($this->owner->{$this->rankField} === null) {
            $this->owner->{$this->rankField} = $this->owner->{$this->defaultValueField};
            $this->owner->setIsNewRecord(false);
            $this->owner->save(false);
        }
        parent::afterSave($event);
    }
}