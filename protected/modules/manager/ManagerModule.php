<?php

Yii::import('admin.AdminExtModule');
class ManagerModule extends AdminExtModule
{
    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array( //			'manager.models.*',
//			'manager.components.*',
        ));
        $this->setLayoutPath(Yii::app()->getModule('admin')->getLayoutPath());
    }

    public static function menuConfig()
    {
        return array(
            'main' => array(
                'label' => 'Главная',
                'plain' => true,
                'items' => array(
                    array(
                        'label' => Yii::app()->settings->get('system', 'siteName'),
                        'url' => array('default/index'),
                        'activateOn' => array(
                            array('route' => '/default\/index/',),
                        ),
                        'roles' => 'admin',
                    ),
                )
            ),

            'site' => array(
                'label' => 'Главная',
                'items' => array(
                    array(
                        'label' => 'Общие параметры',
                        'url' => array('sitePage/index'),
                        'roles' => 'admin',
                    ),

                )
            ),
//            'questions' => array(
//                'label' => 'Вопросы и Ответы',
//                'plain' => false,
//                'items' => array(
//                    array(
//                        'label' => 'Вопросы и Ответы',
//                        'url' => array('question/admin'),
//                        'activateOn' => array(
//                            array('route' => '/question\/\w+/',),
//                        ),
//                        'roles' => 'admin',
//                    ),
//                    array(
//                        'label' => 'Категории Вопросов',
//                        'url' => array('questionCategory/admin'),
//                        'activateOn' => array(
//                            array('route' => '/questionCategory\/\w+/',),
//                        ),
//                        'roles' => 'admin',
//                    ),
//                )
//            ),
//
//            'news' => array(
//                'label' => 'Новости',
//                'plain' => true,
//                'items' => array(
//                    array(
//                        'label' => 'Новости',
//                        'url' => array('news/admin'),
//                        'activateOn' => array(
//                            array('route' => '/news\/\w+/',),
//                        ),
//                        'roles' => 'admin',
//                    ),
//                )
//            ),
//
//
//            'contact' => array(
//                'label' => 'Контакты',
//                'plain' => true,
//                'items' => array(
//                    array(
//                        'label' => 'Контакты',
//                        'url' => array('sitePage/contact'),
//                        'roles' => 'admin',
//                    ),
//                )
//            ),

            'settings' => array(
                'label' => 'Настройки',
                'plain' => false,
                'items' => array(
                    array(
                        'label' => 'Общее',
                        'url' => array('settings/index'),
                        'activateOn' => array(
                            array('route' => '/settings\/index/',),
                        ),
                        'roles' => 'admin',
                    ),
                    array(
                        'label' => 'Соц. сети',
                        'url' => array('settings/soc'),
                        'activateOn' => array(
                            array('route' => '/settings\/soc/',),
                        ),
                        'roles' => 'admin',
                    ),
                    array(
                        'label' => 'Параметры для разработчика',
                        'url' => array('developer/index'),
                        'activateOn' => array(
                            array('route' => '/developer\/index/',),
                        ),
                        'roles' => 'admin',
                    ),
                )
            ),
        );
    }
}
