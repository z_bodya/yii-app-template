<div class="container">
    <div class="row">
        <div class="span12">
            <?php

            /** @var $this DeveloperController */
            $this->beginWidget('CActiveForm', array('action' => array('/admin/manager/developer/clearCache')));
            ?>
            <button type="submit" class="btn">Очистить кеш Twig</button>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>