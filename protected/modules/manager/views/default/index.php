<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3">
            <h3>Страницы</h3>
            <?php

            $this->widget('zii.widgets.CMenu', array(
                'htmlOptions' => array('class' => 'nav nav-tabs nav-stacked'),
                'items' => array(
                    array(
                        'label' => 'Главная',
                        'url' => array('/admin/manager/sitePage/index'),
                    ),
                    array(
                        'label' => 'Контакты',
                        'url' => array('/admin/manager/sitePage/contact'),
                    ),
                ),
                'encodeLabel' => false,
            ));
            ?>
        </div>
        <div class="span3">
            <h3>Разделы</h3>
            <?php

            $this->widget('zii.widgets.CMenu', array(
                'items' => array(
                    array(
                        'label' => 'О компании',
                        'url' => array('/admin/manager/sitePage/about'),
                    ),
                    array(
                        'label' => 'Вопросы и ответы',
                        'url' => array('/admin/manager/question/admin'),
                    ),
                    array(
                        'label' => 'Новости',
                        'url' => array('/admin/manager/news/admin'),
                    ),
                ),
                'htmlOptions' => array('class' => 'nav nav-tabs nav-stacked'),
                'encodeLabel' => false,
            ));
            ?>
        </div>
        <div class="span3">
            <h3>Настройки</h3>
            <?php

            $this->widget('zii.widgets.CMenu', array(
                'items' => array(
                    array(
                        'label' => 'Общие параметры',
                        'url' => array('/admin/manager/settings/index'),
                    ),
                    array(
                        'label' => 'Настройки соц. сетей',
                        'url' => array('/admin/manager/settings/soc'),
                        'roles' => 'admin',
                    ),
                    array(
                        'label' => 'Переводы сообщений',
                        'url' => array('/admin/translate/message/index'),
                    ),                    
                ),
                'htmlOptions' => array('class' => 'nav nav-tabs nav-stacked'),
                'encodeLabel' => false,
            ));
            ?>
        </div>
    </div>
</div>

