<?php
/**
 * Created by PhpStorm.
 * User: z_bodya
 * Date: 12/11/13
 * Time: 7:37 PM
 */
class DeveloperController extends AdminController
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionClearCache()
    {
        if (Yii::app()->request->isPostRequest) {
            Yii::app()->viewRenderer->clearCache();
        }
        $this->redirect(array('index'));
    }
}