<?php
/**
 * Created by JetBrains PhpStorm.
 * User: z_bodya
 * Date: 8/15/13
 * Time: 4:02 AM
 * To change this template use File | Settings | File Templates.
 */

class ApiController extends AdminController
{
    public function actions()
    {
        return array(
            'saveImageAttachment' => 'vendor.z_bodya.yii-image-attachment.ImageAttachmentAction',
        );
    }

}