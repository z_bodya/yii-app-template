<?php

class SettingsController extends AdminController
{
    public $defaultAction = 'index';
    public $layout = 'default';

    public function actions()
    {
        return array(
            'index' => array(
                'class' => 'SettingsEditor',

                'submitLabel' => 'Сохранить',
                'category' => 'system',
                'model' => array(
                    'schema' => array(
                        'adminEmail' => array(
                            'label' => 'Email куда отправляются сообщения с сайта',
                            'default' => 'email@example.com',
                        ),
                        'infoEmail' => array(
                            'label' => 'Email с которого отправляются сообщения с сайта',
                            'default' => 'email@example.com',
                        ),
                        'contactEmail' => array(
                            'label' => 'Email для сообщений со страницы контактов',
                            'default' => 'support@example.com',
                        ),
                        'siteName' => array(
                            'label' => 'Название сайта',
                            'default' => '',
                        ),
                        'gaCode' => array(
                            'label' => 'Код Google Analytics',
                        ),
                        'ymCode' => array(
                            'label' => 'Код Yandex.Metrika',
                        ),
                    ),
                    'rules' => array(),
                ),
                'form' => array(
                    array(
                        'type' => 'fieldset',
                        'legend' => 'Параметры отправки сообщений с сайта',
                        'config' => array(
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'adminEmail',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'infoEmail',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'contactEmail',
                                'options' => array(),
                            ),
                        ),
                    ),
                    array(
                        'type' => 'fieldset',
                        'legend' => 'SEO',
                        'config' => array(
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textArea',
                                'attribute' => 'gaCode',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textArea',
                                'attribute' => 'ymCode',
                                'options' => array(),
                            ),
                        ),
                    ),
                    array(
                        'type' => 'fieldset',
                        'legend' => 'Общие параметры страниц сайта',
                        'config' => array(
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'siteName',
                            ),
                        ),
                    ),
                ),
            ),
            'soc' => array(
                'class' => 'SettingsEditor',

                'submitLabel' => 'Сохранить',
                'category' => 'system',
                'model' => array(
                    'schema' => array(

                        'vk_link' => array(
                            'label' => 'Вконтакте, ссылка',
                        ),
                        'vk_app_id' => array(
                            'label' => 'Вконтакте, APP ID',
                        ),
                        'vk_count' => array(
                            'label' => 'Вконтакте, количество учасников',
                        ),
                        'fb_link' => array(
                            'label' => 'Facebook, ссылка',
                        ),
                        'fb_app_id' => array(
                            'label' => 'Facebook, APP ID',
                        ),
                        'fb_count' => array(
                            'label' => 'Facebook, количество учасников',
                        ),
                        'gp_link' => array(
                            'label' => 'Google Plus, ссылка',
                        ),
                        'gp_count' => array(
                            'label' => 'Google Plus, количество учасников',
                        ),
                        'tw_link' => array(
                            'label' => 'Twitter, ссылка',
                        ),
                        'tw_count' => array(
                            'label' => 'Twitter, количество учасников',
                        ),
                        'ok_link' => array(
                            'label' => 'OK, ссылка',
                        ),
                        'ok_count' => array(
                            'label' => 'OK, количество учасников',
                        ),
                        'ok_client_id' => array(
                            'label' => 'OK, Application ID',
                        ),
                        'ok_client_secret' => array(
                            'label' => 'OK, Секретный ключ приложения',
                        ),
                        'ok_application_key' => array(
                            'label' => 'OK, Публичный ключ приложения',
                        ),
                    ),
                    'rules' => array(),
                ),
                'form' => array(
                    array(
                        'type' => 'fieldset',
                        'legend' => 'Соц. сети',
                        'config' => array(
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'vk_link',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'vk_app_id',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'vk_count',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'fb_link',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'fb_app_id',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'fb_count',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'tw_link',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'tw_count',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'gp_link',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'gp_count',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'ok_link',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'ok_count',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'ok_client_id',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'ok_client_secret',
                                'options' => array(),
                            ),
                            array(
                                'type' => 'simpleInput',
                                'name' => 'textField',
                                'attribute' => 'ok_application_key',
                                'options' => array(),
                            ),
                        ),
                    ),

                ),
            ),
        );
    }
}
