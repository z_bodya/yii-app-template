<?php

class SitePageController extends AdminController
{
    public $layout = 'default';

//    public function actionIndex(){
//        $this->redirect(array('contact'));
//    }
    public function actions()
    {

        $mceSmall = array(
            'settings' => array(
                'menubar' => false,
                'toolbar' => "undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media | forecolor backcolor",
            ),
        );
        $mceFull = array();

        return array(
            'index' => array(
                'class' => 'StaticPageEditor',

                'submitLabel' => 'Сохранить',
                'section' => 'site',
                'key' => 'index',

                'model' => array(
                    'schema' => array(
                        'heading' => array(
                            'label' => 'Заголовок',
                        ),
                        'content' => array(
                            'label' => 'Содержимое',
                        ),
                        'content2' => array(
                            'label' => 'Содержимое 2',
                        ),
                    ),
                ),
                'form' => array(
                    array(
                        'type' => 'simpleInput',
                        'name' => 'textField',
                        'attribute' => 'heading',
                    ),
                    array(
                        'type' => 'inputWidget',
                        'class' => 'TinyMce',
                        'config' => $mceFull,
                        'attribute' => 'content',
                    ),
                    array(
                        'type' => 'inputWidget',
                        'class' => 'TinyMce',
                        'config' => $mceSmall,
                        'attribute' => 'content2',
                    ),
                ),
            ),
        );
    }
}
