#!/bin/sh

. ./defs.sh

for folder in ${systemFolders}
do
    rm -rf ${folder}/*
    chmod -R 777 ${projectRoot}/${folder}
done

for folder in ${contentFolders}
do
    chmod -R 777 ${projectRoot}/${folder}
done
