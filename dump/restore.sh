#!/bin/sh

. ./defs.sh

src=$1
mkdir tmp
cd ./tmp
tar -xvf ../${src}
cd -

echo "Restoring content files"
for folder in ${contentFolders}
do
    echo "Coping tmp/${folder} to ${projectRoot}/${folder}"
    rm -rf ${projectRoot}/${folder}/*
    cp -r ./tmp/${folder}/* ${projectRoot}/${folder}/
done

echo "Restoring database"
mysql -u ${dbUser} -p${dbPass} ${dbName} < tmp/database.sql
#rm -rf tmp

echo "Fixing permissions, removing temporary files"
. ./fix.sh