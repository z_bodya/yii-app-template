#!/bin/sh

. ./defs.sh

dst=`date +"%d%m%y-%H%M%S"`
dstPath=`pwd`/${backupsDir}/${dst}
backupDir=`pwd`/tmp

mkdir ${backupsDir}

echo "Dump content files"
for folder in ${contentFolders}
do
    echo "Coping ${projectRoot}/${folder} to ${backupDir}/${folder}"
    mkdir -p ${backupDir}/${folder}
    cp -r ${projectRoot}/${folder}/* ${backupDir}/${folder}
done

mysqldump -u ${dbUser} -p${dbPass} ${dbName} > ${backupDir}/database.sql
cd ${backupDir}
tar -cvf ${dstPath}.tar ./*
cd -
rm -rf ${backupDir}