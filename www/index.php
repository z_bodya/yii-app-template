<?php
date_default_timezone_set('UTC');

$yii = dirname(__FILE__) . '/../vendor/yiisoft/yii/framework/yii.php';
$composerAutoloader = dirname(__FILE__) . '/../vendor/autoload.php';
$config = dirname(__FILE__) . '/../protected/config/web.php';


defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($composerAutoloader);
require_once($yii);

Yii::setPathOfAlias('vendor', '../vendor');

require_once(dirname(__FILE__).'/../protected/components/Application.php');

Yii::createApplication('Application',$config)->run();
